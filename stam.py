


class stam:
    baseline = (175, 50, 35)
    Points3d_init_file = ("S01_3Ddata_dst_init.csv", "S02_3Ddata_dst_init.csv", "S03_3Ddata_dst_init.csv")
    Points2d_init_file = ("S01_2Ddata_dst_init.csv", "S02_2Ddata_dst_init.csv", "S03_2Ddata_dst_init.csv")
    Intrinsics_file = ("intrinsicsS01.xml", "intrinsicsS02.xml", "intrinsicsS03.xml")
    Next_frame_fmt = ("S01L03_VGA/S01L03_VGA_%04d.png", "S02L03_VGA/S02L03_VGA_%04d.png", "S03L03_VGA/S03L03_VGA_%04d.png")
    
    def __init__(self,scene = 1):
        self.scene = scene
        self.baseline = baseline[self.scene-1]        
        self.points2d_init_file = Points_2d_init_file[self.scene-1]
        self.points3d_init_file = Points_3d_init_file[self.scene-1]
        self.intrinsics_file = Intrinsics_file[self.scene-1]
        self.next_frame_fmt = Next_frame_fmt[self.scene-1]
        self.intrinsic_file_loaded = cv2.cv.Load(self.intrinsics_file)
        
