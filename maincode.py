import numpy as np
import cv2
import sys

def main():
    i = 0
    img_present = True
    while img_present:
        frame_no = i
        img = cv2.imread("S03L03_VGA/S03L03_VGA_%04d.png"%(frame_no))
        if img == None:
            img_present = False
            if i == 0:
                print 'No Images found'
            else:
                print '%d images found. Done'%(i+1)
        else:
            print 'frame number = %04d'%(frame_no)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        scene_no = 1
        print "Using default Scene:%d"%scene_no
    elif len(sys.argv) == 2:
        if int(sys.argv[1]) in (1,2,3):
            scene_no = int(sys.argv[1])
            print "Using Scene %d"%sys.argv[1]
        else:
            sys.exit("Error: Invalid Scene no. provided. Scene no. should be 1,2 or 3")
    else:
        sys.exit("Error: Invalid no of arguments given. len(sys.argv)=%d"%len(sys.argv))
    main()
